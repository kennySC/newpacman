﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pac_man.model
{
    public class Fantasma
    {
        private PictureBox imgFantasma;
        private int direccion;
        private int nextNodo;
        private bool enCuarto;
        private bool enNodoObjetivo;
        private bool muerto;
        private int nodoActual;
        private Boolean enRuta; // estado del fantasma si esta en ruta 
        private Boolean huyendo; // cuando el fantasma esta azul y puede ser comido por pacman
        private int velocidad; // velocidad a la que se va mover el fantasma 
        private string tipoFantasma; // para saber el color del fantasma, si es el rojo, anaranjado o guerever 
                                     // R = rojo, P = pink, O = orange, C = cyan 

        // CONSTRUCTOR VACIO //
        public Fantasma()
        {
            enRuta = true;
            huyendo = true;
            enNodoObjetivo = false;
            muerto = false;
        }

        // CONSTRUCTOR CON PARAMETROS //
        public Fantasma(Boolean _enruta, Boolean _huyendo, Boolean enCuarto, string _tipoFantasma, PictureBox p)
        {
            this.enCuarto = enCuarto;
            enRuta = _enruta;
            huyendo = _huyendo;
            tipoFantasma = _tipoFantasma;
            enNodoObjetivo = false;
            muerto = false;
            switch (tipoFantasma)
            {
                case "R":
                    direccion = 3;
                    velocidad = 2;
                    break;
                case "B":
                    direccion = 4;
                    velocidad = 2;
                    break;
                case "P":
                    direccion = 3;
                    velocidad = 2;
                    break;
                case "O":
                    direccion = 4;
                    velocidad = 3;
                    break;
            }
            imgFantasma = p;
        }

        //  SETS Y GETS //

        public bool isOnNodoObjetivo()
        {
            return enNodoObjetivo;
        }

        public void setOnNodoObjetivo(bool n)
        {
            enNodoObjetivo = n;
        }

        public bool isMuerto()
        {
            return muerto;
        }

        public void setMuerto(bool m)
        {
            muerto = m;
        }

        public bool isEnCuarto()
        {
            return enCuarto;
        }
        public void setEncuarto(bool n)
        {
            enCuarto = n;
        }

        public Boolean getenRuta()
        {
            return enRuta;
        }
        public void setEnRuta(Boolean _enRuta)
        {
            enRuta = _enRuta;
        }

        public Boolean getHuyendo()
        {
            return huyendo;
        }
        public void sethuyendo(Boolean _huyendo)
        {
            huyendo = _huyendo;
        }

        public int getVelocidad()
        {
            return velocidad;
        }
        public void setVelocidad(int _velocidad)
        {
            velocidad = _velocidad;
        }

        public string getTipoFantasma()
        {
            return tipoFantasma;
        }
        public void settipoFantasma(string _tipoFantasma)
        {
            this.tipoFantasma = _tipoFantasma;
        }

        public void setNextNodo(int n)
        {
            nextNodo = n;
        }

        public int getNextNodo()
        {
            return nextNodo;
        }

        public PictureBox getFantasma()
        {
            return imgFantasma;
        } 

        public void setDireccion(int d)
        {
            direccion = d;
        }
        public int getDireccion()
        {
            return direccion;
        }

        

        public int getNodoActual()
        {
            return nodoActual;
        }

        public void setNodoActual(int n)
        {
            nodoActual = n;
        }
    }
}
