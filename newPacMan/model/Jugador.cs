﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pac_man.model
{
    class Jugador
    {
        private string Nombre;
        private bool enNodo;
        private int direccion; // 0.detenido 1.Arriba 2.Abajo 3.Izquierda 4.Derecha
        private bool comer;
        private int posX, posY;
        private int nodoActual;

        public Jugador(string nom)
        {
            this.comer = false;
            this.Nombre = nom;
        }

        public Jugador(int x, int y)
        {
            //enNodo = true;
            comer = false;
            direccion = 0;
            posX = x;
            posY = y;
        }     

        public void setNodoActual(int id)
        {
            nodoActual = id;
        }

        public int getNodoActual()
        {
            return nodoActual;
        }

        public Boolean isComer()
        {
            return comer;
        } 

        public void setComer(Boolean c)
        {
            comer = c;
        }

        public string getnombre()
        {
            return Nombre;
        }

        public void setNombre(string n)
        {
            Nombre = n;
        }

        public bool isEnNodo()
        {
            return enNodo;
        }

        public void setEnNodo(bool en)
        {
            this.enNodo = en;
        }

        public int getDireccion()
        {
            return direccion;
        }

        public void setDireccion(int d)
        {
            direccion = d;
        }

        public int getPosX()
        {
            return posX;
        }

        public void setPosX(int x)
        {
            posX = x;
        }

        public int getPosY()
        {
            return posY;
        }

        public void setPosY(int y)
        {
            posY = y;
        }
    }
}
