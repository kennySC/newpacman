﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_man.model
{
    class Estadistica
    {
        private Jugador jugador; // objeto de la clase jugador 
        private int puntajeTotal;
        private int puntMayPart; // puntaje mayor por partida 
        private int puntMaySPV; // puntaje mayor sin perder una vida 
        private int tiempoTotal; // el tiempo total desde que inicia una partida, sin importar si pasa de nivel
        private int mejorTiempoPart; // el mejor tiempo pero por partida 
        private int cantFantComidos; // la cantidad total de fantasmas comidos 
        private int vidasPerdidas; // la cantidad total de vidas perdidas 
        private int intentosNivel; // la cantidad de veces en un mismo nivel 

        // SETS Y GETS //
        public Jugador getJugador()
        {
            return jugador;
        }
        public void setJugador(Jugador _jugador)
        {
            this.jugador = _jugador;
        }

        public int getPuntajeTotal()
        {
            return puntajeTotal;
        }
        public void setPuntajeTotal(int _puntajetotal)
        {
            this.puntajeTotal = _puntajetotal;
        }

        public int getPuntMayPart()
        {
            return puntMayPart;
        }
        public void setPuntMayPart(int _puntmaypart)
        {
            this.puntMayPart = _puntmaypart;
        }

        public int getPuntMaySPV()
        {
            return puntMaySPV;
        }
        public void setPuntMaySPV(int _puntmayspv)
        {
            this.puntMaySPV = _puntmayspv;
        }

        public int getTiempoTotal()
        {
            return tiempoTotal;
        }
        public void setTiempoTotal(int _tiempototal)
        {
            this.tiempoTotal = _tiempototal;
        }

        public int getMejorTiempoPart()
        {
            return mejorTiempoPart;
        }
        public void setMejorTiempoPart(int _mejortiempopart)
        {
            this.mejorTiempoPart = _mejortiempopart;
        }

        public int getCantFantComidos()
        {
            return cantFantComidos;
        }
        public void setCantFantComidos(int _cantfantcomidos)
        {
            this.cantFantComidos = _cantfantcomidos;
        }

        public int getVidasPerdidas()
        {
            return vidasPerdidas;
        }
        public void setVidasPerdidas(int _vidasperdidas)
        {
            this.vidasPerdidas = _vidasperdidas;
        }

        public int getIntentosNivel()
        {
            return intentosNivel;
        }
        public void setIntentosNivel(int _intentosnivel)
        {
            this.intentosNivel = _intentosnivel;
        }

        // CONSTRUCTOR SIN PARAMETROS //
        public Estadistica(){
        }

        // CONSTRUCTOR CON PARAMETROS //
        public Estadistica(Jugador _jug, int _puntTot, int _puntmaypart, int _puntmayspv, int _tiempotot, int _mejortiempopart, int _cantfantcomidos, int _vidasperdidas, int _intentosnivel)
        {
            this.jugador = _jug;
            this.puntajeTotal = _puntTot;
            this.puntMayPart = _puntmaypart;
            this.puntMaySPV = _puntmayspv;
            this.tiempoTotal = _tiempotot;
            this.mejorTiempoPart = _mejortiempopart;
            this.cantFantComidos = _cantfantcomidos;
            this.vidasPerdidas = _vidasperdidas;
            this.intentosNivel = _intentosnivel;

        }

    }
}
