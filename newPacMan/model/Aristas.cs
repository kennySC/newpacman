﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPacMan.model
{
    class Aristas
    {

        private int distancia;
        private int id;
        private int direccionEnlace;

        public Aristas(int ID, int Distancia, int direccion)
        {
            this.id = ID;
            this.distancia = Distancia;
            this.direccionEnlace = direccion;
        }

        public int getDireccion()
        {
            return direccionEnlace;
        }

        public int getID()
        {
            return id;
        }

        public int getDistancia()
        {
            return distancia;
        }
        
    }
}
