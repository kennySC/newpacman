﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newPacMan.model
{
    class Nodo
    {
        private int fila;
        private int columna;
        private int coordX;
        private int coordY;
        private int tamano;
        private int id;
        private bool nodFantasmas;
        private Rectangle limitesNodo;
        private List<Aristas> ListAristas;        


        public Nodo(int x, int y, int ID, int fil, int col, bool nf) {
            tamano = 24;
            this.coordX = x;
            this.coordY = y;
            this.id = ID;
            this.fila = fil;
            this.columna = col;
            ListAristas = new List<Aristas>();
            limitesNodo = new Rectangle(x, y, tamano, tamano);
            nodFantasmas = nf;
        }

        public List<Aristas> GetAristas()
        {
            return ListAristas;
        }

        public void setAristas(List<Aristas> lAristas)
        {
            this.ListAristas = lAristas;
        }

        public int getCoordX()
        {
            return coordX;
        }

        public int getCoordY()
        {
            return coordY;
        }

        public int getID()
        {
            return id;
        }

        public int getFila()
        {
            return fila;
        }

        public int getColumna()
        {
            return columna;
        }

        public Rectangle getLimitesNodo()
        {
            return limitesNodo;
        }

        public bool getNodFantasma()
        {
            return nodFantasmas;
        }

    }
}
