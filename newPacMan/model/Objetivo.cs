﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pac_man.model
{
    class Objetivo
    {
        private string tipo;
        private int tamanno;
        private PictureBox icono;

        public Objetivo(string tipo)
        {
            this.tipo = tipo;
            tamanno = 5;
            //PictureBox depende, luego se hace;
        }

        public string getTipo()
        {
            return tipo;
        }

        public void setTipo(string t)
        {
            tipo = t;
        }

        public int getTamanno()
        {
            return tamanno;            
        }

        public void setTamanno(int t)
        {
            tamanno = t;
        }

        public PictureBox getIcono()
        {
            return icono;
        }

        public void setIcono(PictureBox i)
        {
            icono = i;
        }

    }
}
