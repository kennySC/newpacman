﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace newPacMan
{
    public partial class PantallaNiveles : Form
    {

        PantallaJugar pj;
        int nivel;
        public PantallaNiveles()
        {
            InitializeComponent();
        }

        // INITIALIZE DEL FORMO DE LA PANTALLA NIVELES //
        private void PantallaNiveles_Load(object sender, EventArgs e)
        { 
        }

        // ********************** ON ACTION PARA LOS BOTONES DE LOS NIVELES ********************** //
        private void btnNivel1_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(1);
            nivel = 1;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel2_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(2);
            nivel = 2;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel3_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(3);
            nivel = 3;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel4_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(4);
            nivel = 4;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel5_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(5);
            nivel = 5;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel6_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(6);
            nivel = 6;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel7_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(7);
            nivel = 7;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel8_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(8);
            nivel = 8;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel9_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(9);
            nivel = 9;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void btnNivel10_Click(object sender, EventArgs e)
        {
            //pj = new PantallaJugar(10);
            nivel = 10;
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }
        // ********************************************************************************** //

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread hilo = new System.Threading.Thread(abrirSiguientePantalla);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void abrirPantallaJugar()
        {
            Application.Run(new PantallaJugar(nivel, 6));
        }

        private void abrirSiguientePantalla()
        {
            Application.Run(new PantallaPrincipal());
        }

        private void cerrandoPantNiveles(object sender, FormClosingEventArgs e)
        {
            this.Controls.Clear();
        }
    }
}
