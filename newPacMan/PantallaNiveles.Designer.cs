﻿namespace newPacMan
{
    partial class PantallaNiveles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaNiveles));
            this.btnNivel1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnNivel2 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnNivel3 = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnNivel5 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnNivel4 = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.btnNivel6 = new System.Windows.Forms.Button();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.btnNivel7 = new System.Windows.Forms.Button();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.btnNivel8 = new System.Windows.Forms.Button();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.btnNivel9 = new System.Windows.Forms.Button();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.btnNivel10 = new System.Windows.Forms.Button();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.btnSalir = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNivel1
            // 
            this.btnNivel1.AutoSize = true;
            this.btnNivel1.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel1.FlatAppearance.BorderSize = 0;
            this.btnNivel1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel1.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel1.Location = new System.Drawing.Point(29, 67);
            this.btnNivel1.Name = "btnNivel1";
            this.btnNivel1.Size = new System.Drawing.Size(85, 36);
            this.btnNivel1.TabIndex = 1;
            this.btnNivel1.Text = "Nivel 1";
            this.btnNivel1.UseVisualStyleBackColor = false;
            this.btnNivel1.Click += new System.EventHandler(this.btnNivel1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::newPacMan.Properties.Resources.temanivel1;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(29, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 85);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // btnNivel2
            // 
            this.btnNivel2.AutoSize = true;
            this.btnNivel2.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel2.FlatAppearance.BorderSize = 0;
            this.btnNivel2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel2.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel2.Location = new System.Drawing.Point(29, 181);
            this.btnNivel2.Name = "btnNivel2";
            this.btnNivel2.Size = new System.Drawing.Size(85, 36);
            this.btnNivel2.TabIndex = 3;
            this.btnNivel2.Text = "Nivel 2";
            this.btnNivel2.UseVisualStyleBackColor = false;
            this.btnNivel2.Click += new System.EventHandler(this.btnNivel2_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::newPacMan.Properties.Resources.temanivel2;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(29, 132);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(194, 85);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // btnNivel3
            // 
            this.btnNivel3.AutoSize = true;
            this.btnNivel3.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel3.FlatAppearance.BorderSize = 0;
            this.btnNivel3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel3.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel3.Location = new System.Drawing.Point(29, 287);
            this.btnNivel3.Name = "btnNivel3";
            this.btnNivel3.Size = new System.Drawing.Size(85, 36);
            this.btnNivel3.TabIndex = 5;
            this.btnNivel3.Text = "Nivel 3";
            this.btnNivel3.UseVisualStyleBackColor = false;
            this.btnNivel3.Click += new System.EventHandler(this.btnNivel3_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::newPacMan.Properties.Resources.temanivel3;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(29, 238);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(194, 85);
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // btnNivel5
            // 
            this.btnNivel5.AutoSize = true;
            this.btnNivel5.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel5.FlatAppearance.BorderSize = 0;
            this.btnNivel5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel5.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel5.Location = new System.Drawing.Point(297, 69);
            this.btnNivel5.Name = "btnNivel5";
            this.btnNivel5.Size = new System.Drawing.Size(85, 36);
            this.btnNivel5.TabIndex = 7;
            this.btnNivel5.Text = "Nivel 5";
            this.btnNivel5.UseVisualStyleBackColor = false;
            this.btnNivel5.Click += new System.EventHandler(this.btnNivel5_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::newPacMan.Properties.Resources.temanivel5;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(297, 20);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(194, 85);
            this.pictureBox4.TabIndex = 8;
            this.pictureBox4.TabStop = false;
            // 
            // btnNivel4
            // 
            this.btnNivel4.AutoSize = true;
            this.btnNivel4.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel4.FlatAppearance.BorderSize = 0;
            this.btnNivel4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel4.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel4.Location = new System.Drawing.Point(29, 390);
            this.btnNivel4.Name = "btnNivel4";
            this.btnNivel4.Size = new System.Drawing.Size(85, 36);
            this.btnNivel4.TabIndex = 9;
            this.btnNivel4.Text = "Nivel 4";
            this.btnNivel4.UseVisualStyleBackColor = false;
            this.btnNivel4.Click += new System.EventHandler(this.btnNivel4_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::newPacMan.Properties.Resources.temanivel4;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(29, 341);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(194, 85);
            this.pictureBox5.TabIndex = 10;
            this.pictureBox5.TabStop = false;
            // 
            // btnNivel6
            // 
            this.btnNivel6.AutoSize = true;
            this.btnNivel6.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel6.FlatAppearance.BorderSize = 0;
            this.btnNivel6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel6.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel6.Location = new System.Drawing.Point(297, 181);
            this.btnNivel6.Name = "btnNivel6";
            this.btnNivel6.Size = new System.Drawing.Size(85, 36);
            this.btnNivel6.TabIndex = 11;
            this.btnNivel6.Text = "Nivel 6";
            this.btnNivel6.UseVisualStyleBackColor = false;
            this.btnNivel6.Click += new System.EventHandler(this.btnNivel6_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::newPacMan.Properties.Resources.temanivel6;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(297, 132);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(194, 85);
            this.pictureBox6.TabIndex = 12;
            this.pictureBox6.TabStop = false;
            // 
            // btnNivel7
            // 
            this.btnNivel7.AutoSize = true;
            this.btnNivel7.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel7.FlatAppearance.BorderSize = 0;
            this.btnNivel7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel7.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel7.Location = new System.Drawing.Point(297, 287);
            this.btnNivel7.Name = "btnNivel7";
            this.btnNivel7.Size = new System.Drawing.Size(85, 36);
            this.btnNivel7.TabIndex = 13;
            this.btnNivel7.Text = "Nivel 7";
            this.btnNivel7.UseVisualStyleBackColor = false;
            this.btnNivel7.Click += new System.EventHandler(this.btnNivel7_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = global::newPacMan.Properties.Resources.temanivel7;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.Location = new System.Drawing.Point(297, 238);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(194, 85);
            this.pictureBox7.TabIndex = 14;
            this.pictureBox7.TabStop = false;
            // 
            // btnNivel8
            // 
            this.btnNivel8.AutoSize = true;
            this.btnNivel8.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel8.FlatAppearance.BorderSize = 0;
            this.btnNivel8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel8.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel8.Location = new System.Drawing.Point(297, 390);
            this.btnNivel8.Name = "btnNivel8";
            this.btnNivel8.Size = new System.Drawing.Size(85, 36);
            this.btnNivel8.TabIndex = 15;
            this.btnNivel8.Text = "Nivel 8";
            this.btnNivel8.UseVisualStyleBackColor = false;
            this.btnNivel8.Click += new System.EventHandler(this.btnNivel8_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = global::newPacMan.Properties.Resources.temanivel8;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox8.Location = new System.Drawing.Point(297, 341);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(194, 85);
            this.pictureBox8.TabIndex = 16;
            this.pictureBox8.TabStop = false;
            // 
            // btnNivel9
            // 
            this.btnNivel9.AutoSize = true;
            this.btnNivel9.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel9.FlatAppearance.BorderSize = 0;
            this.btnNivel9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel9.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel9.Location = new System.Drawing.Point(557, 181);
            this.btnNivel9.Name = "btnNivel9";
            this.btnNivel9.Size = new System.Drawing.Size(85, 36);
            this.btnNivel9.TabIndex = 17;
            this.btnNivel9.Text = "Nivel 9";
            this.btnNivel9.UseVisualStyleBackColor = false;
            this.btnNivel9.Click += new System.EventHandler(this.btnNivel9_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImage = global::newPacMan.Properties.Resources.temanivel9;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox9.Location = new System.Drawing.Point(557, 132);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(194, 85);
            this.pictureBox9.TabIndex = 18;
            this.pictureBox9.TabStop = false;
            // 
            // btnNivel10
            // 
            this.btnNivel10.AutoSize = true;
            this.btnNivel10.BackColor = System.Drawing.Color.Crimson;
            this.btnNivel10.FlatAppearance.BorderSize = 0;
            this.btnNivel10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnNivel10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNivel10.Font = new System.Drawing.Font("Harlow Solid Italic", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNivel10.Location = new System.Drawing.Point(557, 287);
            this.btnNivel10.Name = "btnNivel10";
            this.btnNivel10.Size = new System.Drawing.Size(86, 36);
            this.btnNivel10.TabIndex = 19;
            this.btnNivel10.Text = "Nivel 10";
            this.btnNivel10.UseVisualStyleBackColor = false;
            this.btnNivel10.Click += new System.EventHandler(this.btnNivel10_Click);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImage = global::newPacMan.Properties.Resources.temanivel10;
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox10.Location = new System.Drawing.Point(557, 238);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(194, 85);
            this.pictureBox10.TabIndex = 20;
            this.pictureBox10.TabStop = false;
            // 
            // btnSalir
            // 
            this.btnSalir.AutoSize = true;
            this.btnSalir.BackColor = System.Drawing.Color.Crimson;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(656, 402);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(95, 36);
            this.btnSalir.TabIndex = 21;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // PantallaNiveles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(180)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnNivel10);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.btnNivel9);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.btnNivel8);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.btnNivel7);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.btnNivel6);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.btnNivel4);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.btnNivel5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.btnNivel3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.btnNivel2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnNivel1);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PantallaNiveles";
            this.Text = "PantallaNiveles";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.cerrandoPantNiveles);
            this.Load += new System.EventHandler(this.PantallaNiveles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNivel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnNivel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnNivel3;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnNivel5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button btnNivel4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button btnNivel6;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button btnNivel7;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button btnNivel8;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button btnNivel9;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Button btnNivel10;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Button btnSalir;
    }
}