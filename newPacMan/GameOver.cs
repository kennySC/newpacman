﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace newPacMan
{
    public partial class GameOver : Form
    {
        int segundo = 10;
        public GameOver()
        {
            InitializeComponent();
        }

        private void Continuar_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread hilo = new Thread(abrirPantallaJugar);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void abrirPantallaJugar()
        {
            Application.Run(new PantallaJugar(1, 6));
        }

        private void abrirPantallaPrincipal()
        {
            Application.Run(new PantallaPrincipal());
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread hilo = new Thread(abrirPantallaPrincipal);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void CuentaRegresiva_Tick(object sender, EventArgs e)
        {
            lbCountDown.Text = segundo.ToString();
            segundo--;
            if(segundo == -1)
            {
                this.Close();
                Thread hilo = new Thread(abrirPantallaPrincipal);
                hilo.SetApartmentState(ApartmentState.STA);
                hilo.Start();
            }
        }
    }
}
