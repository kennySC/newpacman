﻿using System;
using Pac_man.model;
using Pac_man.util;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Media;

namespace newPacMan
{
    public partial class PantallaPrincipal : Form
    {
        SoundPlayer player;
        public PantallaPrincipal()
        {
            player = new SoundPlayer(Properties.Resources.menuMusic);
            player.Load();
            player.Play();
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnJugar_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread hilo = new Thread(abrirSiguientePantalla);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void abrirSiguientePantalla()
        {
            Application.Run(new PantallaJugar(1, 6));
        }

        private void abrirPantallaNiveles()
        {
            Application.Run(new PantallaNiveles());
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        // BOTON PARA PASAR EL FORM DE LOS 10 NIVELES //
        private void btnNiveles_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread hilo = new Thread(abrirPantallaNiveles);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void cerrandoPrincipal(object sender, FormClosingEventArgs e)
        {
            this.Controls.Clear();
        }
    }
}
