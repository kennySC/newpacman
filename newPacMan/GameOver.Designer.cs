﻿namespace newPacMan
{
    partial class GameOver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Continuar = new System.Windows.Forms.Button();
            this.Salir = new System.Windows.Forms.Button();
            this.lbCountDown = new System.Windows.Forms.Label();
            this.CuentaRegresiva = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // Continuar
            // 
            this.Continuar.BackColor = System.Drawing.Color.Fuchsia;
            this.Continuar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Continuar.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Continuar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Continuar.Location = new System.Drawing.Point(70, 288);
            this.Continuar.Name = "Continuar";
            this.Continuar.Size = new System.Drawing.Size(110, 50);
            this.Continuar.TabIndex = 0;
            this.Continuar.Text = "Continuar";
            this.Continuar.UseVisualStyleBackColor = false;
            this.Continuar.Click += new System.EventHandler(this.Continuar_Click);
            // 
            // Salir
            // 
            this.Salir.BackColor = System.Drawing.Color.Fuchsia;
            this.Salir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Salir.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Salir.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Salir.Location = new System.Drawing.Point(324, 288);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(110, 50);
            this.Salir.TabIndex = 1;
            this.Salir.Text = "Llorar";
            this.Salir.UseVisualStyleBackColor = false;
            this.Salir.Click += new System.EventHandler(this.Salir_Click);
            // 
            // lbCountDown
            // 
            this.lbCountDown.BackColor = System.Drawing.Color.Transparent;
            this.lbCountDown.Font = new System.Drawing.Font("Harlow Solid Italic", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCountDown.ForeColor = System.Drawing.Color.White;
            this.lbCountDown.Location = new System.Drawing.Point(207, 280);
            this.lbCountDown.Name = "lbCountDown";
            this.lbCountDown.Size = new System.Drawing.Size(94, 82);
            this.lbCountDown.TabIndex = 2;
            this.lbCountDown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CuentaRegresiva
            // 
            this.CuentaRegresiva.Enabled = true;
            this.CuentaRegresiva.Interval = 1000;
            this.CuentaRegresiva.Tick += new System.EventHandler(this.CuentaRegresiva_Tick);
            // 
            // GameOver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::newPacMan.Properties.Resources.gameOver;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(502, 379);
            this.Controls.Add(this.lbCountDown);
            this.Controls.Add(this.Salir);
            this.Controls.Add(this.Continuar);
            this.Name = "GameOver";
            this.Text = "GameOver";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Continuar;
        private System.Windows.Forms.Button Salir;
        private System.Windows.Forms.Label lbCountDown;
        private System.Windows.Forms.Timer CuentaRegresiva;
    }
}