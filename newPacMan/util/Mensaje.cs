﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pac_man.util
{
    class Mensaje
    {
        public Mensaje()
        {
        }

        public void show(string title, string mensaje, MessageBoxIcon type)
        {
            if(type == MessageBoxIcon.Information)
            {
                MessageBox.Show(mensaje, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if(type == MessageBoxIcon.Warning)
            {
                MessageBox.Show(mensaje, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if(type == MessageBoxIcon.Error)
            {
                MessageBox.Show(mensaje, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool showConfirmation(string title, string question)
        {
            return (MessageBox.Show(Form.ActiveForm, question, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);
        }


        //Se supone que este es MODAL pero pero por algun motivo el de arriba tambien es modal XD
        public void showModal(string title, string mensaje, MessageBoxIcon type)
        {
            if (type == MessageBoxIcon.Information)
            {
                MessageBox.Show(Form.ActiveForm, mensaje, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (type == MessageBoxIcon.Warning)
            {
                MessageBox.Show(Form.ActiveForm, mensaje, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (type == MessageBoxIcon.Error)
            {
                MessageBox.Show(Form.ActiveForm, mensaje, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (type == MessageBoxIcon.Asterisk)
            {
                MessageBox.Show(Form.ActiveForm, mensaje, title, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
    }
}
