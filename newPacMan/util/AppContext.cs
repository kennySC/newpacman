﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_man.util
{
    class AppContext
    {
        private AppContext() { }
        private static Dictionary<string, Object> context = new Dictionary<string,Object>();
        private static AppContext _INSTANCE;
        public static AppContext INSTANCE
        {
            get
            {
                if (_INSTANCE == null)
                {
                   _INSTANCE = new AppContext();
                }
                return _INSTANCE;
            }
        }

        public Object get(string parameter)
        {
            try
            {
                Object value = context[parameter];
                return value;
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }

        public void set(string parameter, Object value)
        {
            if(parameter != null && value != null)
            {
                context[parameter] = value;
            }
            else
            {
                Console.Error.WriteLine("Clave o valor nulo al ingresar de AppContext");
            }
        }

        public void delete(string parameter)
        {
            try
            {
                context.Remove(parameter);
            }
            catch (ArgumentNullException)
            {
                Console.Error.WriteLine("Clave nula al eliminar de AppContext");
            }
        }
    }
}
