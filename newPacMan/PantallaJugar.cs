﻿using newPacMan.model;
using Pac_man.model;
using Pac_man.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace newPacMan
{
    public partial class PantallaJugar : Form
    {

        Nivel nivel;
        Jugador jugador;
        PictureBox pbPacman;
        List<PictureBox> puntos;
        List<PictureBox> powerUps;
        List<Rectangle> muros;
        List<PictureBox> fantasmas;
        List<Nodo> listNodos;
        List<Fantasma> listFantasmas;
        List<PictureBox> puntosEspeciales;
        Stack<string> pilaFantasmas;        
        Flecha flecha;
        int velPacman;
        int velFantas;
        int puntaje = 0;
        int contadorSegundos = 0, cont = 0;
        string[,] map;
        Nodo[,] matrizNodo;
        bool parte1;
        Fantasma fantRojo = new Fantasma();
        Fantasma fantAzul = new Fantasma();
        Fantasma fantRosa = new Fantasma();
        Fantasma fantNaranja = new Fantasma();
        int sigNivel, idNodFant = 0;
         public int[,] matrizPeso;
        int ObjetivoX, ObjetivoY;
        bool band1 = true, band2 = true;
        Stack<int> ruta;
        SoundPlayer sonidoPuntosEsp;
        SoundPlayer sonidoMuerto;
        SoundPlayer sonidoMovimiento1;
        SoundPlayer sonidoMovimiento2;
        SoundPlayer sonidoOpening;
        SoundPlayer sonidoSirena;
        SoundPlayer sonidoComerFantasma;
        SoundPlayer sonidoIntermision;
        int Xorig = 0;
        int Yorig = 0;
        List<int> listamayorRojo;
        List<int> listamayorRosa;
        List<int> listaRojo;
        List<int> listaRosa;
        int sumRojo = 9999;
        int sumRosa = 9999;
        int tiempoComer = 0;
        int cantidadVidas;
        Random RanRosa = new Random();
        Random RanAzul = new Random();
        Random RanNaranja = new Random();

        int objetivoRojo;
        int objetivoRosa;
        int objetivoAzul;
        int objetivoNaranja;

        List<PictureBox> vidas;
        // CONSTRUCTOR INCIAL, "EL INITIALIZE" //
        public PantallaJugar(int numNivel, int cantVidas)
        {
            cantidadVidas = cantVidas;
            flecha = new Flecha();
            pbPacman = new PictureBox();                           
            puntos = new List<PictureBox>();
            powerUps = new List<PictureBox>();
            muros = new List<Rectangle>();
            fantasmas = new List<PictureBox>();
            listNodos = new List<Nodo>();
            vidas = new List<PictureBox>();
            listFantasmas = new List<Fantasma>();
            puntosEspeciales = new List<PictureBox>();
            pilaFantasmas = new Stack<string>();            
            pilaFantasmas.Push("O");
            pilaFantasmas.Push("P");            
            pilaFantasmas.Push("B");
            pilaFantasmas.Push("R");
            nivel = new Nivel(numNivel);
            map = nivel.geteStrucNivel();
            leerLaberinto();
            crearListaAdyacencia();
            this.BackgroundImage = nivel.getFondo();
            InitializeComponent();            
            cargarElementos();
            ImpListaAdyacencia();
            velPacman = 2;
            velFantas = 2;
            parte1 = true;
            

            ruta = new Stack<int>();

            sonidoOpening = new SoundPlayer(Properties.Resources.Pacman_Opening_Song);
            sonidoOpening.Play();
            sonidoPuntosEsp = new SoundPlayer(Properties.Resources.Pacman_Eating_Cherry);
            sonidoPuntosEsp.Load();
            sonidoMuerto = new SoundPlayer(Properties.Resources.Pacman_Dies);
            sonidoMuerto.Load();
            sonidoMovimiento1 = new SoundPlayer(Properties.Resources.waka_p1);
            sonidoMovimiento1.Load();
            sonidoMovimiento2 = new SoundPlayer(Properties.Resources.waka_p2);
            sonidoMovimiento2.Load();
            sonidoComerFantasma = new SoundPlayer(Properties.Resources.Pacman_Eating_Ghost);
            sonidoComerFantasma.Load();            
            sonidoIntermision = new SoundPlayer(Properties.Resources.Pacman_Intermission);
            sonidoIntermision.Load();
            sonidoSirena = new SoundPlayer(Properties.Resources.Pacman_Siren);
            sonidoSirena.Load();

            matrizPeso = new int[listNodos.Count, listNodos.Count];
            matrizNodo = new Nodo[listNodos.Count, listNodos.Count];
            crearMatrizPeso();
            crearMatrizNodos();

            for (int i = 0; i < listFantasmas.Count; i++)
            {
                if (listFantasmas[i].getTipoFantasma().Equals("R"))
                {
                    fantRojo = listFantasmas[i];                    
                }
                if (listFantasmas[i].getTipoFantasma().Equals("B"))
                {
                    fantAzul = listFantasmas[i];
                }
                if (listFantasmas[i].getTipoFantasma().Equals("P"))
                {
                    fantRosa = listFantasmas[i];
                }
                if (listFantasmas[i].getTipoFantasma().Equals("O"))
                {
                    fantNaranja = listFantasmas[i];
                }
            }

        }

        //  Elemento de tipo enumeracion que funciona para el buffer del movimiento
        public enum Flecha : int
        {
            Arriba = 1,
            Abajo = 2,
            Izquierda = 3,
            Derecha = 4, 
            No = 5
        }
        
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread hilo = new Thread(abrirSiguientePantalla);
            hilo.SetApartmentState(ApartmentState.STA);
            hilo.Start();
        }

        //  Metodo que regresa a la pantalla principal
        private void abrirSiguientePantalla()
        {
            Application.Run(new PantallaPrincipal());
        }

        //  Metodo que abre la pantalla de game over
        private void abrirpantallaGameOver()
        {
            Application.Run(new GameOver());
        }

        //  Metodo que carga el siguiente nivel 
        private void abrirSiguienteNivel()
        {
            Application.Run(new PantallaJugar(sigNivel, cantidadVidas));
        }

        //  Metodo que lee el nivel desde una matriz
        private void leerLaberinto()
        {
            int x = 30;
            int y = 30;
            int idNodo = 0;
            string colorF;
            int tam = nivel.getTamanoCelda();
            
            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 31; j++)
                {
                    if (map[i, j].Equals("W"))  //  Dibujamos el Laberinto
                    {
                        Rectangle rect = new Rectangle(x, y, tam, tam);                        
                        muros.Add(rect);
                    }
                    if (map[i, j].Equals("I") || map[i, j].Equals("IO") || map[i, j].Equals("IP") || map[i, j].Equals("IF"))
                    {
                        if(map[i, j].Equals("IF"))
                        {
                            listNodos.Add(new Nodo(x, y, idNodo, i, j, true));
                            idNodFant = idNodo;
                        }
                        else
                        {
                            listNodos.Add(new Nodo(x, y, idNodo, i, j, false));
                        }
                        idNodo++;
                    }
                    if (map[i, j].Equals("F") || map[i, j].Equals("IF"))  //  Fantasmas
                    {
                        bool enCuarto = true;
                        colorF = pilaFantasmas.Pop();
                        PictureBox fantasma = new PictureBox();
                        if (colorF.Equals("R")) { fantasma.Image = Properties.Resources.rojoLeft; enCuarto = false; }
                        if (colorF.Equals("B")) { fantasma.Image = Properties.Resources.azulUp; }
                        if (colorF.Equals("P")) { fantasma.Image = Properties.Resources.rosaUp; }
                        if (colorF.Equals("O")) { fantasma.Image = Properties.Resources.naranjaUp; }
                        fantasma.SizeMode = PictureBoxSizeMode.StretchImage;
                        fantasma.BackColor = Color.Transparent;
                        fantasma.Width = tam;
                        fantasma.Height = tam;
                        fantasma.Location = new Point(x, y);                        
                        Fantasma fan = new Fantasma(true, false, enCuarto, colorF, fantasma);
                        if (map[i, j].Equals("IF"))
                        {
                            fan.setNodoActual(idNodo-1);
                        }
                        listFantasmas.Add(fan);
                        fantasmas.Add(fan.getFantasma());
                        this.Controls.Add(fantasma);
                    }
                    x += tam;
                }
                x = 30;
                y += tam;
            }
               
        }

        //  Metodo que pinta y/o repinta el laberinto
        private void pintarLaberinto(object sender, PaintEventArgs e)
        {
            for(int i = 0; i < muros.Count; i++)
            {
                SolidBrush brush = new SolidBrush(nivel.getColorMuros());
                e.Graphics.FillRectangle(brush, muros[i]);
            }
        }

        //  Metodo que carga los elementos del nivel
        public void cargarElementos()
        {
            int x = 30;
            int y = 30;
            int tam = nivel.getTamanoCelda();

            String colorF;            
            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 31; j++)
                {
                    if (map[i, j].Equals("IP") || map[i, j].Equals("P")) //  Pacman
                    {
                        pbPacman.Image = Properties.Resources.pacDer;
                        pbPacman.BackColor = Color.Transparent;
                        pbPacman.SizeMode = PictureBoxSizeMode.StretchImage;
                        pbPacman.Width = tam;
                        pbPacman.Height = tam;
                        pbPacman.Location = new Point(x, y);
                        Xorig = x;
                        Yorig = y;

                        jugador = new Jugador(x, y);
                        this.Controls.Add(pbPacman);
                    }
                    if (map[i, j].Equals(" ") /*|| map[i, j].Equals("I")*/)  //  Puntos
                    {
                        PictureBox punto = new PictureBox();
                        punto.Image = Properties.Resources.ball;
                        punto.BackColor = Color.Transparent;
                        punto.SizeMode = PictureBoxSizeMode.StretchImage;
                        punto.Width = tam-10;
                        punto.Height = tam-10;
                        punto.Location = new Point(x + 5, y + 5);
                        puntos.Add(punto);
                        this.Controls.Add(punto);
                    }
                    if (map[i, j].Equals("O") || map[i, j].Equals("IO"))  //  PowerUps
                    {
                        PictureBox powerUp = new PictureBox();
                        powerUp.Image = Properties.Resources.powerUpIMG;
                        powerUp.BackColor = Color.Transparent;
                        powerUp.Width = tam;
                        powerUp.Height = tam;
                        powerUp.Location = new Point(x, y);
                        powerUps.Add(powerUp);
                        this.Controls.Add(powerUp);
                    }
                    if(map[i, j].Equals("S"))
                    {
                        PictureBox puntoEsp = new PictureBox();
                        switch (nivel.getNumNivel())
                        {
                            case 1:
                                puntoEsp.Image = Properties.Resources.joker;
                                break;
                            case 2:
                                puntoEsp.Image = Properties.Resources.simpsons;
                                break;
                            case 3:
                                puntoEsp.Image = Properties.Resources.ironman;
                                break;
                            case 4:
                                puntoEsp.Image = Properties.Resources.spiderman;
                                break;
                            case 5:
                                puntoEsp.Image = Properties.Resources.batman;
                                break;
                            case 6:
                                puntoEsp.Image = Properties.Resources.superman;
                                break;
                            case 7:
                                puntoEsp.Image = Properties.Resources.deadpool;
                                break;
                            case 8:
                                puntoEsp.Image = Properties.Resources.onepiece;
                                break;
                            case 9:
                                puntoEsp.Image = Properties.Resources.naruto;
                                break;
                            case 10:
                                puntoEsp.Image = Properties.Resources.dragonball;
                                break;
                        }
                        puntoEsp.BackColor = Color.Transparent;
                        puntoEsp.SizeMode = PictureBoxSizeMode.StretchImage;
                        puntoEsp.Width = tam - 10;
                        puntoEsp.Height = tam - 10;
                        puntoEsp.Location = new Point(x + 5, y + 5);
                        puntosEspeciales.Add(puntoEsp);
                        this.Controls.Add(puntoEsp);
                    }
                    x += tam;
                }
                x = 30;
                y += tam;
            }
            // CARGAMOS LAS 6 VIDAS DEL JUGADOR //
            int coordX = 225;
            for (int i = 0; i < cantidadVidas; i++)
            {
                PictureBox vidasPacMan = new PictureBox();
                vidasPacMan.Image = Properties.Resources.vidaspacman;
                vidasPacMan.BackColor = Color.Transparent;
                vidasPacMan.SizeMode = PictureBoxSizeMode.StretchImage;
                vidasPacMan.Width = tam - 10;
                vidasPacMan.Height = tam - 10;
                vidasPacMan.Location = new Point(coordX, 412);
                vidas.Add(vidasPacMan);
                this.Controls.Add(vidasPacMan);
                coordX = coordX + 25;
            }
        }

        //  Metodo que convierte la lista de adyacencia una matriz de pesos
        private void crearMatrizPeso()
        {
            int k = 0, l;
            while (k < listNodos.Count)
            {
                l = 0;
                while (l < listNodos[k].GetAristas().Count)
                {
                    matrizPeso[listNodos[k].getID(), listNodos[k].GetAristas()[l].getID()] = listNodos[k].GetAristas()[l].getDistancia();
                    l++;
                }
                k++;
            }

            for (int i = 0; i < listNodos.Count; i++)
            {
                for (int j = 0; j < listNodos.Count; j++)
                {
                    if (matrizPeso[i, j] == 0)
                    {
                        if (i == j)
                        {
                            matrizPeso[i, j] = 0;
                        }
                        else
                        {
                            matrizPeso[i, j] = 999999;
                        }
                    }
                }
            }

            //for (int i = 0; i < listNodos.Count; i++)
            //{
            //    for (int j = 0; j < listNodos.Count; j++)
            //    {

            //        Console.WriteLine(i + " " + j + " " + matrizPeso[i, j]);


            //    }
            //}

        }

        //  Metodo que obtiene la tecla presionada
        private void keyPressed(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.W)
            {
                flecha = Flecha.Arriba;
            }
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.S)
            {
                flecha = Flecha.Abajo;
            }
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
            {
                flecha = Flecha.Izquierda;
            }
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
            {
                flecha = Flecha.Derecha;
            }
        }

        //  Metodo que hace un framerate estable en la pantalla de juego
        private void pantJugLoad(object sender, EventArgs e)
        {
            this.DoubleBuffered = true;
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer,
                true);
        }

        //  Timer o hilo del movimiento del pacman y sus interaciones con el nivel
        private void tmrPacman_Tick(object sender, EventArgs e)
        {            
            moverPacmanGrafo();
            //moverFantasmasGrafo();
            comer();
            colisionFantasmas();
        }        

        //  Metodo que utiliza el grafo para mover al pacman
        private void moverPacmanGrafo()
        {

            jugador.setEnNodo(isPacmanOnNodo());            
            if (jugador.isEnNodo())
            {
                for (int i = 0; i < listNodos.Count; i++)
                {
                    if (jugador.getPosX() == listNodos[i].getCoordX() && jugador.getPosY() == listNodos[i].getCoordY())
                    {
                        if (flecha == Flecha.Arriba)
                        {
                            if(!isMuro(jugador.getPosX(), jugador.getPosY() - 24))
                            {
                                cambioDireccionPacman(1);
                            }
                            else
                            {
                                switch (jugador.getDireccion())
                                {
                                    case 1:
                                        if (!isMuro(jugador.getPosX(), jugador.getPosY() - 24))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(jugador.getPosX(), jugador.getPosY() + 24))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(jugador.getPosX() - 24, jugador.getPosY()))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(jugador.getPosX() + 24, jugador.getPosY()))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                }
                            }
                            jugador.setEnNodo(false);
                        }
                        else if(flecha == Flecha.Abajo)
                        {
                            if (!isMuro(jugador.getPosX(), jugador.getPosY() + 24))
                            {
                                cambioDireccionPacman(2);
                            }
                            else
                            {
                                switch (jugador.getDireccion())
                                {
                                    case 1:
                                        if (!isMuro(jugador.getPosX(), jugador.getPosY() - 24))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(jugador.getPosX(), jugador.getPosY() + 24))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(jugador.getPosX() - 24, jugador.getPosY()))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(jugador.getPosX() + 24, jugador.getPosY()))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                }
                            }
                            jugador.setEnNodo(false);
                        }
                        else if(flecha == Flecha.Izquierda)
                        {
                            if (!isMuro(jugador.getPosX() - 24, jugador.getPosY()))
                            {
                                cambioDireccionPacman(3);
                            }
                            else
                            {
                                switch (jugador.getDireccion())
                                {
                                    case 1:
                                        if (!isMuro(jugador.getPosX(), jugador.getPosY() - 24))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(jugador.getPosX(), jugador.getPosY() + 24))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(jugador.getPosX() - 24, jugador.getPosY()))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(jugador.getPosX() + 24, jugador.getPosY()))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                }
                            }
                            jugador.setEnNodo(false);
                        }
                        else if (flecha == Flecha.Derecha)
                        {
                            if (!isMuro(jugador.getPosX() + 24, jugador.getPosY()))
                            {
                                cambioDireccionPacman(4);
                            }
                            else
                            {
                                switch (jugador.getDireccion())
                                {
                                    case 1:
                                        if (!isMuro(jugador.getPosX(), jugador.getPosY() - 24))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(jugador.getPosX(), jugador.getPosY() + 24))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(jugador.getPosX() - 24, jugador.getPosY()))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(jugador.getPosX() + 24, jugador.getPosY()))
                                        {
                                            cambioDireccionPacman(jugador.getDireccion());
                                        }
                                        else
                                        {
                                            cambioDireccionPacman(0);
                                        }
                                        break;
                                }
                            }
                            jugador.setEnNodo(false);
                        }                      
                    }
                }
            }
            else
            {
                switch (jugador.getDireccion())
                {                
                case 1: // Direccion hacia Arriba
                    if(flecha == Flecha.Abajo)
                    {                                                                             
                        cambioDireccionPacman(2);   //  Llamamos al metodo que cambia la direccion de pacman
                    }
                    else
                    {
                        cambioDireccionPacman(1);
                    }
                    break;
                case 2: // Direccion hacia Abajo
                    if (flecha == Flecha.Arriba)
                    {
                        cambioDireccionPacman(1);                                                           
                    }
                    else
                    {
                        cambioDireccionPacman(2);
                    }
                    break;
                case 3: //  Direccion hacia la izquierda
                    if (flecha == Flecha.Derecha)
                    {
                        cambioDireccionPacman(4);
                    }
                    else
                    {
                        cambioDireccionPacman(3);
                    }
                    break;
                case 4:
                    if (flecha == Flecha.Izquierda)
                    {
                        cambioDireccionPacman(3);
                    }
                    else
                    {
                        cambioDireccionPacman(4);
                    }
                    break;
                }
            }
        }        

        //  Metodo que comprueba si ha un muero en tal posicion (x, y)
        public bool isMuro(int x, int y)
        {
            for(int i = 0; i < muros.Count; i++)
            {
                if (muros[i].Location.X == x && muros[i].Location.Y == y)
                {
                    return true;
                }
            }
            return false;
        }

        //  Metodo mover fantasmas grafo
        public void MoverFantasmasGrafo(Fantasma fantasma, int dir)
        {                      
            int dirActual = fantasma.getDireccion();
            int posX = fantasma.getFantasma().Location.X, posY = fantasma.getFantasma().Location.Y;
            fantasma.setEnRuta(!isFantasmaOnNodo(fantasma));
            if (!fantRojo.getenRuta())
            {
                for (int i = 0; i < listNodos.Count; i++)
                {
                    if (posX == listNodos[i].getCoordX() && posY == listNodos[i].getCoordY())
                    {
                        if(dirActual == 0)
                        {
                            switch (dir)
                            {
                                case 1:
                                    if (!isMuro(posX, posY - 24))
                                    {
                                        CambioDireccionFantasmas(fantasma, dir);
                                    }
                                    else
                                    {
                                        CambioDireccionFantasmas(fantasma, 0);
                                    }
                                    break;
                                case 2:
                                    if (!isMuro(posX, posY + 24))
                                    {
                                        CambioDireccionFantasmas(fantasma, dir);
                                    }
                                    else
                                    {
                                        CambioDireccionFantasmas(fantasma, 0);
                                    }
                                    break;
                                case 3:
                                    if (!isMuro(posX - 24, posY))
                                    {
                                        CambioDireccionFantasmas(fantasma, dir);
                                    }
                                    else
                                    {
                                        CambioDireccionFantasmas(fantasma, 0);
                                    }
                                    break;
                                case 4:
                                    if (!isMuro(posX + 24, posY))
                                    {
                                        CambioDireccionFantasmas(fantasma, dir);
                                    }
                                    else
                                    {
                                        CambioDireccionFantasmas(fantasma, 0);
                                    }
                                    break;
                            }
                        }
                        else if (dirActual == 1)
                        {
                            if (!isMuro(posX, posY - 24))
                            {
                                switch (dir)
                                {
                                    case 1:
                                        if (!isMuro(posX, posY - 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(posX, posY + 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(posX - 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(posX + 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                switch (dir)
                                {
                                    case 1:
                                        if (!isMuro(posX, posY - 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(posX, posY + 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(posX - 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(posX + 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                }
                            }
                            fantasma.setEnRuta(true);
                        }
                        else if (dirActual == 2)
                        {
                            if (!isMuro(posX, posY + 24))
                            {
                                switch (dir)
                                {
                                    case 1:
                                        if (!isMuro(posX, posY - 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(posX, posY + 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(posX - 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(posX + 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                switch (dir)
                                {
                                    case 1:
                                        if (!isMuro(posX, posY - 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(posX, posY + 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(posX - 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(posX + 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                }
                            }
                            fantasma.setEnRuta(true);
                        }
                        else if (dirActual == 3)
                        {
                            if (!isMuro(posX - 24, posY))
                            {
                                switch (dir)
                                {
                                    case 1:
                                        if (!isMuro(posX, posY - 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(posX, posY + 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(posX - 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(posX + 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                switch (dir)
                                {
                                    case 1:
                                        if (!isMuro(posX, posY - 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(posX, posY + 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(posX - 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(posX + 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                }
                            }
                            fantasma.setEnRuta(true);
                        }
                        else if (dirActual == 4)
                        {
                            if (!isMuro(posX + 24, posY))
                            {
                                switch (dir)
                                {
                                    case 1:
                                        if (!isMuro(posX, posY - 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(posX, posY + 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(posX - 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(posX + 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                switch (dir)
                                {
                                    case 1:
                                        if (!isMuro(posX, posY - 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 2:
                                        if (!isMuro(posX, posY + 24))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 3:
                                        if (!isMuro(posX - 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                    case 4:
                                        if (!isMuro(posX + 24, posY))
                                        {
                                            CambioDireccionFantasmas(fantasma, dir);
                                        }
                                        else
                                        {
                                            CambioDireccionFantasmas(fantasma, 0);
                                        }
                                        break;
                                }
                            }
                            fantasma.setEnRuta(true);
                        }
                    }
                }
            }
            else
            {
                switch (dirActual)
                {
                    case 1:
                        if (!isMuro(posX, posY - 24))
                        {
                            CambioDireccionFantasmas(fantasma, dirActual);
                        }
                        else
                        {
                            CambioDireccionFantasmas(fantasma, 0);
                        }
                        break;
                    case 2:
                        if (!isMuro(posX, posY + 24))
                        {
                            CambioDireccionFantasmas(fantasma, dirActual);
                        }
                        else
                        {
                            CambioDireccionFantasmas(fantasma, 0);
                        }
                        break;
                    case 3:
                        if (!isMuro(posX - 24, posY))
                        {
                            CambioDireccionFantasmas(fantasma, dirActual);
                        }
                        else
                        {
                            CambioDireccionFantasmas(fantasma, 0);
                        }
                        break;
                    case 4:
                        if (!isMuro(posX + 24, posY))
                        {
                            CambioDireccionFantasmas(fantasma, dirActual);
                        }
                        else
                        {
                            CambioDireccionFantasmas(fantasma, 0);
                        }
                        break;
                }
            }
        }

        //  Metodo para comer los puntos y los powerUps y evaluar si ya vencio el nivel, es decir, se comio todos los puntos 
        public void comer()
        {
            for (int i = 0; i < puntos.Count(); i++)
            {
                if (pbPacman.Bounds.IntersectsWith(puntos[i].Bounds))
                {
                    this.Controls.Remove(puntos[i]);
                    puntos.Remove(puntos[i]);
                    sonidoMovimiento1.Play();
                    puntaje += 10;  
                    break;                    
                }
            }
            for (int i = 0; i < powerUps.Count(); i++)
            {
                if (pbPacman.Bounds.IntersectsWith(powerUps[i].Bounds))
                {
                    //velPacman = 3;
                    this.Controls.Remove(powerUps[i]);
                    powerUps.Remove(powerUps[i]);
                    tiempoComer = 0;
                    fantRosa.sethuyendo(true);
                    fantRojo.sethuyendo(true);
                    fantAzul.sethuyendo(true);
                    fantNaranja.sethuyendo(true);
                    tmrComer.Enabled = true;
                    tmrComer.Start();
                    break;
                }
            }
            for (int i = 0; i < puntosEspeciales.Count(); i++)
            {
                if (pbPacman.Bounds.IntersectsWith(puntosEspeciales[i].Bounds))
                {
                    puntaje += nivel.getNumNivel() * 100;
                    this.Controls.Remove(puntosEspeciales[i]);
                    puntosEspeciales.Remove(puntosEspeciales[i]);
                    sonidoPuntosEsp.Play();
                    break;
                }
            }
            //sonidoMovimiento.Stop();
            lbMostrarPuntos.Text = puntaje.ToString();
            bool nivelFinalizado = false;
            if (!puntos.Any() && !puntosEspeciales.Any() && !powerUps.Any())
            {
                tmrBlue.Stop();
                tmrOrange.Stop();
                tmrPacman.Stop();
                tmrPink.Stop();
                tmrRed.Stop();
                Mensaje finPartida = new Mensaje();
                nivelFinalizado = finPartida.showConfirmation("Nivel Finalizado!", "Desea continuar al siguiente nivel?");
                if(nivelFinalizado){
                    this.Close();                    
                    switch (nivel.getNumNivel())
                    {
                        case 1:
                            sigNivel = 2;
                            break;
                        case 2:
                            sigNivel = 3;
                            break;
                        case 3:
                            sigNivel = 4;
                            break;
                        case 4:
                            sigNivel = 5;
                            break;
                        case 5:
                            sigNivel = 6;
                            break;
                        case 6:
                            sigNivel = 7;
                            break;
                        case 7:
                            sigNivel = 8;
                            break;
                        case 8:
                            sigNivel = 9;
                            break;
                        case 9:
                            sigNivel = 10;
                            break;
                    }
                    Thread hilo = new Thread(abrirSiguienteNivel);
                    hilo.SetApartmentState(ApartmentState.STA);
                    hilo.Start();
                }
            }
        }

        //  Metodo para manejar las colisiones con los muros
        public bool colisionMuros()
        {
            bool choca = false;
            for (int i = 0; i < muros.Count(); i++)
            {
                if (pbPacman.Bounds.IntersectsWith(muros[i]))
                {
                    if (flecha == Flecha.Derecha)
                    {
                        pbPacman.Location = new Point(muros[i].Left - 24, pbPacman.Location.Y);
                    }
                    if (flecha == Flecha.Izquierda)
                    {
                        pbPacman.Location = new Point(muros[i].Right, pbPacman.Location.Y);
                    }
                    if (flecha == Flecha.Arriba)
                    {
                        pbPacman.Location = new Point(pbPacman.Location.X, muros[i].Bottom);
                    }
                    if (flecha == Flecha.Abajo)
                    {
                        pbPacman.Location = new Point(pbPacman.Location.X, muros[i].Top - 24);
                    }
                    flecha = Flecha.No;
                    jugador.setDireccion(0);
                    choca = true;
                    break;
                }
            }
            return choca;
        }

        //  Metodo para manejar las colisiones con los muros
        public bool colisionMurosFantasma(PictureBox fantasma, int dir)
        {
            bool choca = false;
            for (int i = 0; i < muros.Count(); i++)
            {
                if (fantasma.Bounds.IntersectsWith(muros[i]))
                {
                    if (dir == 4)
                    {
                        fantasma.Location = new Point(muros[i].Left - 24, pbPacman.Location.Y);
                    }
                    if (dir == 3)
                    {
                        fantasma.Location = new Point(muros[i].Right, pbPacman.Location.Y);
                    }
                    if (dir == 1)
                    {
                        fantasma.Location = new Point(pbPacman.Location.X, muros[i].Bottom);
                    }
                    if (dir == 2)
                    {
                        fantasma.Location = new Point(pbPacman.Location.X, muros[i].Top - 24);
                    }
                    dir = 0;
                    jugador.setDireccion(0);
                    choca = true;
                    break;
                }
            }
            return choca;
        }

        //  Metodo para manejar las colisiones con los fantasmas
        public void colisionFantasmas()
        {
            for(int i = 0; i < listFantasmas.Count; i++)
            {
                if (pbPacman.Bounds.IntersectsWith(listFantasmas[i].getFantasma().Bounds))
                {
                    if (listFantasmas[i].getHuyendo())
                    {
                        listFantasmas[i].getFantasma().Location = new Point(listNodos[idNodFant].getCoordX(), listNodos[idNodFant].getCoordY());
                        listFantasmas[i].sethuyendo(false);
                        listFantasmas[i].setDireccion(4);
                        break;
                    }
                    flecha = Flecha.No;
                    pbPacman.Image = Properties.Resources.pacMuerto;
                    sonidoMuerto.Play();
                    tmrPacman.Stop();
                    tmrRed.Stop();
                    tmrPink.Stop();
                    tmrOrange.Stop();
                    tmrBlue.Stop();
                    timer1.Stop();
                    tmrInicio.Stop();
                    tmrSacarFantasmas.Stop();                    
                    this.Controls.Remove(vidas[vidas.Count - 1]);
                    vidas.RemoveAt(vidas.Count - 1);
                    cantidadVidas--;
                    if(vidas.Count == 0) 
                    {
                        this.Close();
                        Thread hilo = new Thread(abrirpantallaGameOver);
                        hilo.SetApartmentState(ApartmentState.STA);
                        hilo.Start();
                    }
                    tmrReiniciar.Enabled = true;
                    tmrReiniciar.Start();
                }
            }
        }

        //  Metodo que cambia la direccion de pacaman 
        public void cambioDireccionPacman(int nuevaDir)
        {
            bool choca = colisionMuros();
            switch (nuevaDir)
            {
                case 0:
                    flecha = Flecha.No;
                    jugador.setDireccion(0);

                    break;

                case 1:
                    if (!choca)
                    {
                        jugador.setDireccion(1);
                        pbPacman.Image = Properties.Resources.pacUp;
                        pbPacman.BackColor = Color.Transparent;
                        if (pbPacman.Location.Y > 30)
                        {
                            pbPacman.Location = new Point(pbPacman.Location.X, pbPacman.Location.Y - velPacman);
                        }
                        else
                        {
                            pbPacman.Location = new Point(pbPacman.Location.X, 390);
                        }                        
                    }

                    break;

                case 2:

                    if (!choca)
                    {
                        jugador.setDireccion(2);
                        pbPacman.Image = Properties.Resources.pacDown;
                        pbPacman.BackColor = Color.Transparent;
                        if (pbPacman.Location.Y < 390)
                        {
                            pbPacman.Location = new Point(pbPacman.Location.X, pbPacman.Location.Y + velPacman);
                        }
                        else
                        {
                            pbPacman.Location = new Point(pbPacman.Location.X, 30);
                        }
                    }

                    break;

                case 3:

                    if (!choca)
                    {
                        jugador.setDireccion(3);
                        pbPacman.Image = Properties.Resources.pacIzq;
                        pbPacman.BackColor = Color.Transparent;
                        if (pbPacman.Location.X > 30)
                        {
                            pbPacman.Location = new Point(pbPacman.Location.X - velPacman, pbPacman.Location.Y);
                        }
                        else
                        {
                            pbPacman.Location = new Point(774, pbPacman.Location.Y);
                        }
                    }

                    break;

                case 4:

                    if (!choca)
                    {
                        jugador.setDireccion(4);
                        pbPacman.Image = Properties.Resources.pacDer;
                        pbPacman.BackColor = Color.Transparent;
                        if (pbPacman.Location.X < 774)
                        {
                            pbPacman.Location = new Point(pbPacman.Location.X + velPacman, pbPacman.Location.Y);
                        }
                        else
                        {
                            pbPacman.Location = new Point(30, pbPacman.Location.Y);
                        }
                    }

                    break;                
            }
            jugador.setPosX(pbPacman.Location.X);
            jugador.setPosY(pbPacman.Location.Y);
        }

        //  Cambio de direccion para los fantasmas
        public void CambioDireccionFantasmas(Fantasma fant, int nuevaDir)
        {
            PictureBox fantasmaPB = fant.getFantasma();
            bool choca = colisionMurosFantasma(fantasmaPB, nuevaDir);
            switch (nuevaDir)
            {
                case 0:
                    if (!choca)
                    {
                        fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y);
                    }

                    break;

                case 1:
                    if (!choca)
                    {
                        if (!fant.getHuyendo())
                        {
                            switch (fant.getTipoFantasma())
                            {
                                case "R":
                                    fant.getFantasma().Image = Properties.Resources.rojoUp;
                                    break;
                                case "P":
                                    fant.getFantasma().Image = Properties.Resources.rosaUp;
                                    break;
                                case "B":
                                    fant.getFantasma().Image = Properties.Resources.azulUp;
                                    break;
                                case "O":
                                    fant.getFantasma().Image = Properties.Resources.naranjaUp;
                                    break;
                            }
                            if (fantasmaPB.Location.Y > 30)
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y - fant.getVelocidad());
                            }
                            else
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X, 390);
                            }
                        }
                        else if (fant.isMuerto())
                        {
                            
                            fant.getFantasma().Image = Properties.Resources.mup;
                            if (fantasmaPB.Location.Y > 30)
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y - fant.getVelocidad());
                            }
                            else
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X, 390);
                            }
                        }
                        else if(fant.getHuyendo())
                        {
                            if (tiempoComer < 6)
                            {
                                fant.getFantasma().Image = Properties.Resources.comer;
                                if (fantasmaPB.Location.Y > 30)
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y - fant.getVelocidad());
                                }
                                else
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X, 390);
                                }
                            }
                            else if (tiempoComer >= 6 && tiempoComer < 10)
                            {
                                fant.getFantasma().Image = Properties.Resources.Fuera;
                                if (fantasmaPB.Location.Y > 30)
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y - fant.getVelocidad());
                                }
                                else
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X, 390);
                                }
                            }
                        }                        
                    }

                    break;

                case 2:

                    if (!choca)
                    {
                        if (!fant.getHuyendo())
                        {
                            switch (fant.getTipoFantasma())
                            {
                                case "R":
                                    fant.getFantasma().Image = Properties.Resources.rojoDown;
                                    break;
                                case "P":
                                    fant.getFantasma().Image = Properties.Resources.rosaDown;
                                    break;
                                case "B":
                                    fant.getFantasma().Image = Properties.Resources.azulDown;
                                    break;
                                case "O":
                                    fant.getFantasma().Image = Properties.Resources.naranjaDown;
                                    break;
                            }
                            if (fantasmaPB.Location.Y < 390)
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y + fant.getVelocidad());
                            }
                            else
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X, 30);
                            }
                        }
                        else if (fant.isMuerto())
                        {
                            fant.getFantasma().Image = Properties.Resources.mertoDown;
                            if (fantasmaPB.Location.Y < 390)
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y + fant.getVelocidad());
                            }
                            else
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X, 30);
                            }
                        }
                        else if(fant.getHuyendo())
                        {                            
                            if (tiempoComer < 6)
                            {
                                fant.getFantasma().Image = Properties.Resources.comer;
                                if (fantasmaPB.Location.Y < 390)
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y + fant.getVelocidad());
                                }
                                else
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X, 30);
                                }
                            }
                            else if (tiempoComer >= 6 && tiempoComer < 10)
                            {
                                fant.getFantasma().Image = Properties.Resources.Fuera;
                                if (fantasmaPB.Location.Y < 390)
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X, fantasmaPB.Location.Y + fant.getVelocidad());
                                }
                                else
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X, 30);
                                }
                            }                            
                        }
                    }

                    break;

                case 3:

                    if (!choca)
                    {
                        if (!fant.getHuyendo())
                        {
                            switch (fant.getTipoFantasma())
                            {
                                case "R":
                                    fant.getFantasma().Image = Properties.Resources.rojoLeft;
                                    break;
                                case "P":
                                    fant.getFantasma().Image = Properties.Resources.rosaIzq;
                                    break;
                                case "B":
                                    fant.getFantasma().Image = Properties.Resources.azulIzq;
                                    break;
                                case "O":
                                    fant.getFantasma().Image = Properties.Resources.naranjaIzq;
                                    break;
                            }
                            if (fantasmaPB.Location.X > 30)
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X - fant.getVelocidad(), fantasmaPB.Location.Y);
                            }
                            else
                            {
                                fantasmaPB.Location = new Point(774, fantasmaPB.Location.Y);
                            }
                        }
                        else if (fant.isMuerto())
                        {
                            fant.getFantasma().Image = Properties.Resources.mertoIzq;
                            if (fantasmaPB.Location.X > 30)
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X - fant.getVelocidad(), fantasmaPB.Location.Y);
                            }
                            else
                            {
                                fantasmaPB.Location = new Point(774, fantasmaPB.Location.Y);
                            }
                        }
                        else if(fant.getHuyendo())
                        {
                            if (tiempoComer < 6)
                            {
                                fant.getFantasma().Image = Properties.Resources.comer;
                                if (fantasmaPB.Location.X > 30)
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X - fant.getVelocidad(), fantasmaPB.Location.Y);
                                }
                                else
                                {
                                    fantasmaPB.Location = new Point(774, fantasmaPB.Location.Y);
                                }
                            }
                            else if (tiempoComer >= 6 && tiempoComer < 10)
                            {
                                fant.getFantasma().Image = Properties.Resources.Fuera;
                                if (fantasmaPB.Location.X > 30)
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X - fant.getVelocidad(), fantasmaPB.Location.Y);
                                }
                                else
                                {
                                    fantasmaPB.Location = new Point(774, fantasmaPB.Location.Y);
                                }
                            }
                        }
                    }

                    break;

                case 4:

                    if (!choca)
                    {
                        if (!fant.getHuyendo())
                        {
                            switch (fant.getTipoFantasma())
                            {
                                case "R":
                                    fant.getFantasma().Image = Properties.Resources.rojoUp;
                                    break;
                                case "P":
                                    fant.getFantasma().Image = Properties.Resources.rosaUp;
                                    break;
                                case "B":
                                    fant.getFantasma().Image = Properties.Resources.azulUp;
                                    break;
                                case "O":
                                    fant.getFantasma().Image = Properties.Resources.naranjaUp;
                                    break;
                            }
                            if (fantasmaPB.Location.X < 774)
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X + fant.getVelocidad(), fantasmaPB.Location.Y);
                            }
                            else
                            {
                                fantasmaPB.Location = new Point(30, fantasmaPB.Location.Y);
                            }
                        }
                        else if (fant.isMuerto())
                        {
                            fant.getFantasma().Image = Properties.Resources.mertoDer;
                            if (fantasmaPB.Location.X < 774)
                            {
                                fantasmaPB.Location = new Point(fantasmaPB.Location.X + fant.getVelocidad(), fantasmaPB.Location.Y);
                            }
                            else
                            {
                                fantasmaPB.Location = new Point(30, fantasmaPB.Location.Y);
                            }
                        }
                        else if(fant.getHuyendo())
                        {
                            if (tiempoComer < 6)
                            {
                                fant.getFantasma().Image = Properties.Resources.comer;
                                if (fantasmaPB.Location.X < 774)
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X + fant.getVelocidad(), fantasmaPB.Location.Y);
                                }
                                else
                                {
                                    fantasmaPB.Location = new Point(30, fantasmaPB.Location.Y);
                                }
                            }
                            else if (tiempoComer >= 6 && tiempoComer < 10)
                            {
                                fant.getFantasma().Image = Properties.Resources.Fuera;
                                if (fantasmaPB.Location.X < 774)
                                {
                                    fantasmaPB.Location = new Point(fantasmaPB.Location.X + fant.getVelocidad(), fantasmaPB.Location.Y);
                                }
                                else
                                {
                                    fantasmaPB.Location = new Point(30, fantasmaPB.Location.Y);
                                }
                            }
                        }
                    }

                    break;
            }
            fant.setDireccion(nuevaDir);
        }

        //  Metodo que crea la lista de adyacencia para manejar el movimiento de pacman y de los fantasmas
        public void crearListaAdyacencia()
        {
            int tam = nivel.getTamanoCelda();
            int x, y, fil, col, distancia = 0, idNodo;            

            for (int i = 0; i < listNodos.Count; i++)
            {
                //  Conexion a la izquierda del nodo
                distancia = 0;
                fil = listNodos[i].getFila();
                col = listNodos[i].getColumna()-1;
                x = listNodos[i].getCoordX()-tam;
                y = listNodos[i].getCoordY();
                if (col > 1)
                {
                    while (!map[fil, col].Equals("W"))
                    {
                        if (map[fil, col].Equals("I") || map[fil, col].Equals("IO") || map[fil, col].Equals("IP") || map[fil, col].Equals("IF"))
                        {
                            idNodo = getIDNodo(x, y);
                            listNodos[i].GetAristas().Add(new Aristas(idNodo, distancia, 3));
                            break;
                        }
                        if(col == 0) { break; }
                        x -= tam;
                        distancia++;
                        col--;
                    }
                }

                //  Conexion a la derecha del nodo
                distancia = 1;
                fil = listNodos[i].getFila();
                col = listNodos[i].getColumna() + 1;
                x = listNodos[i].getCoordX() + tam;
                y = listNodos[i].getCoordY();
                if (col < 31)
                {
                    while (!map[fil, col].Equals("W"))
                    {
                        if (map[fil, col].Equals("I") || map[fil, col].Equals("IO") || map[fil, col].Equals("IP") || map[fil, col].Equals("IF"))
                        {
                            idNodo = getIDNodo(x, y);
                            listNodos[i].GetAristas().Add(new Aristas(idNodo, distancia, 4));
                            break;
                        }
                        if (col == 30) { break; }
                        x += tam;
                        distancia++;
                        col++;
                    }
                }

                //  Conexion arriba del nodo
                distancia = 1;
                fil = listNodos[i].getFila() - 1;
                col = listNodos[i].getColumna();
                x = listNodos[i].getCoordX();
                y = listNodos[i].getCoordY() - tam;
                if (fil > 0)
                {
                    while (!map[fil, col].Equals("W"))
                    {
                        if (map[fil, col].Equals("I") || map[fil, col].Equals("IO") || map[fil, col].Equals("IP") || map[fil, col].Equals("IF"))
                        {
                            idNodo = getIDNodo(x, y);
                            listNodos[i].GetAristas().Add(new Aristas(idNodo, distancia, 1));
                            break;
                        }
                        if (fil == 0) { break; }
                        y -= tam;
                        distancia++;
                        fil--;
                    }
                }

                //  Conexion abajo del nodo
                distancia = 1;
                fil = listNodos[i].getFila() + 1;
                col = listNodos[i].getColumna();
                x = listNodos[i].getCoordX();
                y = listNodos[i].getCoordY() + tam;
                if (fil < 15)
                {
                    while (!map[fil, col].Equals("W"))
                    {
                        if (map[fil, col].Equals("I") || map[fil, col].Equals("IO") || map[fil, col].Equals("IP") || map[fil, col].Equals("IF"))
                        {
                            idNodo = getIDNodo(x, y);
                            listNodos[i].GetAristas().Add(new Aristas(idNodo, distancia, 2));
                            break;
                        }
                        if (fil == 14) { break; }
                        y += tam;
                        distancia++;
                        fil++;
                    }
                }
            }

        }

        // Metodo que obtiene el id del nodo al que se conecta uno
        public int getIDNodo(int x, int y)
        {
            for(int i = 0; i < listNodos.Count; i++)
            {
                if(listNodos[i].getCoordX() == x && listNodos[i].getCoordY() == y)
                {
                    return listNodos[i].getID();
                }                
            }
            return 'N';
        }

        //  Metodo que imprime la lista de adyacencia en la consola
        public void ImpListaAdyacencia()
        {
            for(int i = 0; i < listNodos.Count; i++)
            {
                Console.Write("Nodo: " + listNodos[i].getID() + " ");
                for (int j = 0; j < listNodos[i].GetAristas().Count; j++)
                {
                    Console.Write("->" + listNodos[i].GetAristas()[j].getID() + " - Direccion: " + listNodos[i].GetAristas()[j].getDireccion() + " | ");
                }
                Console.WriteLine();
            }
        }

        //  Metodo que comprueba cuando el pacman esta en un nodo del grafo
        public bool isPacmanOnNodo()
        {
            for(int i = 0; i < listNodos.Count; i++)
            {
                if(pbPacman.Location.X == listNodos[i].getCoordX() && pbPacman.Location.Y == listNodos[i].getCoordY())
                {
                    if (!listNodos[i].getNodFantasma())
                    {
                        jugador.setNodoActual(listNodos[i].getID());
                        return true;
                    }                    
                }
            }
            return false;
        }

        //  Metodo que comprueba cuando un fantasma esta en un nodo del grafo
        public bool isFantasmaOnNodo(Fantasma fant)
        {
            PictureBox fantasma = fant.getFantasma();
            for (int i = 0; i < listNodos.Count; i++)
            {
                if (fantasma.Location.X == listNodos[i].getCoordX() && fantasma.Location.Y == listNodos[i].getCoordY())
                {
                    fant.setNodoActual(listNodos[i].getID());
                    if (listNodos[i].getNodFantasma() && fant.isMuerto())
                    {
                        return true;
                    }
                    else if(listNodos[i].getNodFantasma() && !fant.isMuerto())
                    {
                        return false;
                    }
                    else if (!listNodos[i].getNodFantasma()){
                        return true;
                    }
                }
            }
            return false;
        }

        private void CerrarVentana(object sender, FormClosingEventArgs e)
        {
            this.Controls.Clear();
            tmrPacman.Enabled = false;
            tmrInicio.Enabled = false;
            tmrRed.Enabled = false;
            tmrBlue.Enabled = false;
            tmrPink.Enabled = false;
            tmrOrange.Enabled = false;
            sonidoOpening.Stop();
            this.components.Dispose();
        }

        private void tmrPrincipal(object sender, EventArgs e)
        {
            lbPreparado.Visible = false;
            tmrInicio.Stop();
            tmrPacman.Enabled = true;
            tmrPacman.Start();
            tmrRed.Enabled = true;
            tmrRed.Start();
            timer1.Enabled = true;
            timer1.Start();
        }

        private void sacarFantasmas(object sender, EventArgs e)
        {

            if (fantRosa.isEnCuarto())
            {
                if (band1)
                {
                    ObjetivoX = fantRosa.getFantasma().Location.X;
                    ObjetivoY = fantRosa.getFantasma().Location.Y - 72;
                    band1 = false;
                }
                else
                {
                    if (fantRosa.getFantasma().Location.Y != ObjetivoY)
                    {
                        fantRosa.getFantasma().Location = new Point(ObjetivoX, fantRosa.getFantasma().Location.Y - 2);
                    }
                    else
                    {
                        fantRosa.setEncuarto(false);
                        tmrPink.Enabled = true;
                        tmrPink.Start();
                        tmrSacarFantasmas.Stop();
                        timer1.Start();
                        band1 = true;
                    }
                }
            }
            else
            {
                if (fantNaranja.isEnCuarto())
                {
                    if (band1)
                    {
                        fantNaranja.getFantasma().Image = Properties.Resources.naranjaIzq;
                        ObjetivoX = fantNaranja.getFantasma().Location.X - 24;
                        ObjetivoY = fantNaranja.getFantasma().Location.Y;
                        band1 = false;
                    }
                    else
                    {
                        if (fantNaranja.getFantasma().Location.X != ObjetivoX)
                        {
                            fantNaranja.getFantasma().Location = new Point(fantNaranja.getFantasma().Location.X - 2, ObjetivoY);
                        }
                        else
                        {
                            if (band2)
                            {
                                fantNaranja.getFantasma().Image = Properties.Resources.naranjaUp;
                                ObjetivoX = fantNaranja.getFantasma().Location.X;
                                ObjetivoY = fantNaranja.getFantasma().Location.Y - 72;
                                band2 = false;
                            }
                            else
                            {
                                if (fantNaranja.getFantasma().Location.Y != ObjetivoY)
                                {
                                    fantNaranja.getFantasma().Location = new Point(ObjetivoX, fantNaranja.getFantasma().Location.Y - 2);
                                }
                                else
                                {
                                    fantNaranja.setEncuarto(false);
                                    tmrOrange.Enabled = true;
                                    tmrOrange.Start();
                                    tmrSacarFantasmas.Stop();
                                    timer1.Start();
                                    band1 = true;
                                    band2 = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (fantAzul.isEnCuarto())
                    {
                        if (band1)
                        {
                            fantAzul.getFantasma().Image = Properties.Resources.azulDer;
                            ObjetivoX = fantAzul.getFantasma().Location.X + 24;
                            ObjetivoY = fantAzul.getFantasma().Location.Y;
                            band1 = false;
                        }
                        else
                        {
                            if (fantAzul.getFantasma().Location.X != ObjetivoX)
                            {
                                fantAzul.getFantasma().Location = new Point(fantAzul.getFantasma().Location.X + 2, ObjetivoY);
                            }
                            else
                            {
                                if (band2)
                                {
                                    fantAzul.getFantasma().Image = Properties.Resources.azulUp;
                                    ObjetivoX = fantAzul.getFantasma().Location.X;
                                    ObjetivoY = fantAzul.getFantasma().Location.Y - 72;
                                    band2 = false;
                                }
                                else
                                {
                                    if (fantAzul.getFantasma().Location.Y != ObjetivoY)
                                    {
                                        fantAzul.getFantasma().Location = new Point(ObjetivoX, fantAzul.getFantasma().Location.Y - 2);
                                    }
                                    else
                                    {
                                        fantAzul.setEncuarto(false);
                                        tmrBlue.Enabled = true;
                                        tmrBlue.Start();
                                        tmrSacarFantasmas.Stop();
                                        band1 = true;
                                        band2 = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //  Timer para el movimiento del fantasma rojo
        private void tmrFantasmaRed_Tick(object sender, EventArgs e)
        {
            if (!fantRojo.isMuerto())
            {
                int dir = recorridoFleud(jugador.getNodoActual(), fantRojo.getNodoActual());
                MoverFantasmasGrafo(fantRojo, dir);
            }
            else
            {
                int dir = recorridoFleud(objetivoRojo, fantRojo.getNodoActual());
                MoverFantasmasGrafo(fantRojo, dir);
                if (fantRojo.getFantasma().Location.X == listNodos[idNodFant].getCoordX() && fantRojo.getFantasma().Location.Y == listNodos[idNodFant].getCoordY())
                {
                    fantRojo.setMuerto(false);
                }
            }                   
            
        }

        private void tmrBlueMover(object sender, EventArgs e)
        {
            if (!fantAzul.isMuerto()) { 
                int dir = RanAzul.Next(1, 4);
                MoverFantasmasGrafo(fantAzul, dir);
            }
            else
            {
                int dir = recorridoFleud(objetivoAzul, fantAzul.getNodoActual());
                MoverFantasmasGrafo(fantAzul, dir);
                if(fantAzul.getFantasma().Location.X == listNodos[idNodFant].getCoordX() && fantAzul.getFantasma().Location.Y == listNodos[idNodFant].getCoordY())
                {
                    fantAzul.setMuerto(false);
                }
            }
        }

        private void tmrOrangeMover(object sender, EventArgs e)
        {
            if (!fantNaranja.isMuerto())
            {
                int dir = RanNaranja.Next(1, 4);
                MoverFantasmasGrafo(fantNaranja, dir);
            }
            else
            {
                int dir = recorridoFleud(objetivoNaranja, fantNaranja.getNodoActual());
                MoverFantasmasGrafo(fantNaranja, dir);
                if (fantNaranja.getFantasma().Location.X == listNodos[idNodFant].getCoordX() && fantNaranja.getFantasma().Location.Y == listNodos[idNodFant].getCoordY())
                {
                    fantNaranja.setMuerto(false);
                }
            }            
        }

        private void tmrPinkMover(object sender, EventArgs e)
        {
            if (!fantRosa.isMuerto())
            {
                int dir = recorridoFleud(jugador.getNodoActual(), fantRosa.getNodoActual());
                MoverFantasmasGrafo(fantRosa, dir);
            }
            else
            {
                int dir = recorridoFleud(objetivoRosa, fantRosa.getNodoActual());
                MoverFantasmasGrafo(fantRosa, dir);
                if (fantRosa.getFantasma().Location.X == listNodos[idNodFant].getCoordX() && fantRosa.getFantasma().Location.Y == listNodos[idNodFant].getCoordY())
                {
                    fantRosa.setMuerto(false);
                }
            }            

        }

        private void tmrReiniciarNivel(object sender, EventArgs e)
        {
            tmrSacarFantasmas.Stop();            
            tmrSacarFantasmas.Enabled = false;            
            sonidoOpening.Play();
            jugador.setPosX(Xorig);
            jugador.setPosY(Yorig);
            pbPacman.Location = new Point(Xorig, Yorig);
            pbPacman.Image = Properties.Resources.pacDer;
            recolocarFantasmas();
            recolocarAll();
            lbPreparado.Visible = true;
            tmrInicio.Enabled = true;
            tmrInicio.Start();
            tmrReiniciar.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tmrSacarFantasmas.Enabled = true;
            tmrSacarFantasmas.Start();
            band1 = true;
            band2 = true;
        }

        private void crearMatrizNodos()
        {

            for (int i = 0; i < listNodos.Count; i++)
            {
                for (int j = 0; j < listNodos.Count; j++)
                {
                    matrizNodo[i, j] = listNodos[j];
                }
            }

            for (int k = 0; k < listNodos.Count; k++)
            {
                for (int i = 0; i < listNodos.Count; i++)
                {
                    for (int j = 0; j < listNodos.Count; j++)
                    {
                        if (matrizPeso[i, k] + matrizPeso[k, j] < matrizPeso[i, j])
                        {
                            matrizPeso[i, j] = matrizPeso[i, k] + matrizPeso[k, j];

                            matrizNodo[i, j] = listNodos[k];

                        }

                    }
                }
            }



        }

        private void tmrComiendo(object sender, EventArgs e)
        {
            if (tiempoComer < 6)
            {
                fantRojo.getFantasma().Image = Properties.Resources.comer;
                fantRosa.getFantasma().Image = Properties.Resources.comer;
                fantAzul.getFantasma().Image = Properties.Resources.comer;
                fantNaranja.getFantasma().Image = Properties.Resources.comer;
            }
            if(tiempoComer >= 6 && tiempoComer < 10)
            {
                fantRojo.getFantasma().Image = Properties.Resources.Fuera;
                fantRosa.getFantasma().Image = Properties.Resources.Fuera;
                fantAzul.getFantasma().Image = Properties.Resources.Fuera;
                fantNaranja.getFantasma().Image = Properties.Resources.Fuera;
            }
            if(tiempoComer > 10)
            {
                velPacman = 2;
                fantRojo.sethuyendo(false);
                fantRosa.sethuyendo(false);
                fantAzul.sethuyendo(false);
                fantNaranja.sethuyendo(false);
                fantRojo.getFantasma().Image = Properties.Resources.rojoDer;
                fantRosa.getFantasma().Image = Properties.Resources.rosaDer;
                fantAzul.getFantasma().Image = Properties.Resources.azulDer;
                fantNaranja.getFantasma().Image = Properties.Resources.naranjaDer;
                tmrComer.Stop();
                tmrComer.Enabled = false;
            }
            tiempoComer++;
        }

        public int recorridoFleud(int destino, int posicion)
        {
            Stack<int> recorrido = new Stack<int>();
            int dest = 0;
                dest=destino;
            int cont = 0, id, dir = 0;
            while (cont <= listNodos.Count && dest != posicion)
            {
                Console.WriteLine(dest + " "+posicion);
                id = matrizNodo[dest, posicion].getID();
                for(int i = 0; i < listNodos[posicion].GetAristas().Count; i++)
                {

                    if(listNodos[posicion].GetAristas()[i].getID() == id)
                    {
                        dir = listNodos[posicion].GetAristas()[i].getDireccion();
                        break;
                    }
                }
                recorrido.Push(dir);
                dest = matrizNodo[dest, posicion].getID();
                cont++;
            }

            if (dir == 0)
            {
                Random r = new Random();
                dir = r.Next(1, 4);
            }
            return dir;
        }

        public void recolocarFantasmas()
        {
            listFantasmas.Clear();

            this.Controls.Remove(fantRojo.getFantasma());
            this.Controls.Remove(fantRosa.getFantasma());
            this.Controls.Remove(fantAzul.getFantasma());
            this.Controls.Remove(fantNaranja.getFantasma());            

            fantRojo = new Fantasma();
            fantRosa = new Fantasma();
            fantAzul = new Fantasma();
            fantNaranja = new Fantasma();

            pilaFantasmas.Push("O");
            pilaFantasmas.Push("P");
            pilaFantasmas.Push("B");
            pilaFantasmas.Push("R");

            int x = 30, y = 30;
            int tam = 24;
            for (int i = 0; i < 15; i++)
            {
                for(int j = 0; j < 31; j++)
                {


                    if(map[i, j].Equals("F") || map[i, j].Equals("IF"))
                    {
                        
                        string colorF;
                        bool enCuarto = true;
                        colorF = pilaFantasmas.Pop();
                        PictureBox fantasma = new PictureBox();
                        if (colorF.Equals("R")) { fantasma.Image = Properties.Resources.rojoLeft; enCuarto = false; }
                        if (colorF.Equals("B")) { fantasma.Image = Properties.Resources.azulUp; enCuarto = true; }
                        if (colorF.Equals("P")) { fantasma.Image = Properties.Resources.rosaUp; enCuarto = true; }
                        if (colorF.Equals("O")) { fantasma.Image = Properties.Resources.naranjaUp; enCuarto = true; }
                        fantasma.SizeMode = PictureBoxSizeMode.StretchImage;
                        fantasma.BackColor = Color.Transparent;
                        fantasma.Width = tam;
                        fantasma.Height = tam;
                        fantasma.Location = new Point(x, y);
                        Fantasma fan = new Fantasma(true, false, enCuarto, colorF, fantasma);
                        if (map[i, j].Equals("IF"))
                        {
                            fan.setNodoActual(idNodFant);
                        }
                        listFantasmas.Add(fan);
                        fantasmas.Add(fan.getFantasma());
                        this.Controls.Add(fantasma);
                    }
                    x += tam;
                }
                x = 30;
                y += tam;
            }

            for (int i = 0; i < listFantasmas.Count; i++)
            {
                if (listFantasmas[i].getTipoFantasma().Equals("R"))
                {
                    fantRojo = listFantasmas[i];
                }
                if (listFantasmas[i].getTipoFantasma().Equals("B"))
                {
                    fantAzul = listFantasmas[i];
                }
                if (listFantasmas[i].getTipoFantasma().Equals("P"))
                {
                    fantRosa = listFantasmas[i];
                }
                if (listFantasmas[i].getTipoFantasma().Equals("O"))
                {
                    fantNaranja = listFantasmas[i];
                }
            }

        }

        public void recolocarAll()
        {
            for(int i = 0; i < puntos.Count; i++)
            {
                this.Controls.Remove(puntos[i]);
            }
            for (int i = 0; i < powerUps.Count; i++)
            {
                this.Controls.Remove(powerUps[i]);
            }
            for (int i = 0; i < puntosEspeciales.Count; i++)
            {
                this.Controls.Remove(puntosEspeciales[i]);
            }

            for (int i = 0; i < puntos.Count; i++)
            {
                this.Controls.Add(puntos[i]);
            }
            for (int i = 0; i < powerUps.Count; i++)
            {
                this.Controls.Add(powerUps[i]);
            }
            for (int i = 0; i < puntosEspeciales.Count; i++)
            {
                this.Controls.Add(puntosEspeciales[i]);
            }

        }

   

    public void recorridoDijkstraRojo(int ini, int fin, int x)
    {
        bool band = false;
        for (int i = 0; i < listaRojo.Count; i++)
        {
            if (ini == listaRojo[i])
            {
                band = true;
            }
        }
        //insertar(ini);
        listaRojo.Add(ini);
        if (!band)
        {
            if (ini == fin)
            {
                if (x < sumRojo)
                {
                    sumRojo = x;
                    listamayorRojo.Clear();

                    for (int i = 0; i < listaRojo.Count - 1; i++)
                    {
                        for (int j = 0; j < listNodos[listaRojo[i]].GetAristas().Count; j++)
                        {
                            if (listNodos[listaRojo[i]].GetAristas()[j].getID() == listaRojo[i + 1])
                            {
                                listamayorRojo[i] = listNodos[listaRojo[i]].GetAristas()[j].getDireccion();
                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < listNodos.Count; i++)
                {
                    if (listNodos[i].getID() == ini)
                    {
                        for (int j = 0; j < listNodos[i].GetAristas().Count; j++)
                        {
                            if (listNodos[i].GetAristas()[j].getDistancia() > 0)
                            {
                                recorridoDijkstraRojo(listNodos[i].GetAristas()[j].getID(), fin, x + listNodos[i].GetAristas()[j].getDistancia());
                            }
                        }
                    }
                }
            }
        }

        listaRojo.RemoveAt(listaRojo.Count - 1);
    }

}
}
