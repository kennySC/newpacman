﻿namespace newPacMan
{
    partial class PantallaJugar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaJugar));
            this.btnSalir = new System.Windows.Forms.Button();
            this.tmrPacman = new System.Windows.Forms.Timer(this.components);
            this.label = new System.Windows.Forms.Label();
            this.lbMostrarPuntos = new System.Windows.Forms.Label();
            this.tmrRed = new System.Windows.Forms.Timer(this.components);
            this.tmrInicio = new System.Windows.Forms.Timer(this.components);
            this.tmrBlue = new System.Windows.Forms.Timer(this.components);
            this.tmrPink = new System.Windows.Forms.Timer(this.components);
            this.tmrOrange = new System.Windows.Forms.Timer(this.components);
            this.lbPreparado = new System.Windows.Forms.Label();
            this.tmrSacarFantasmas = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tmrReiniciar = new System.Windows.Forms.Timer(this.components);
            this.tmrComer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.AutoSize = true;
            this.btnSalir.BackColor = System.Drawing.Color.Crimson;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic);
            this.btnSalir.Location = new System.Drawing.Point(803, 391);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(119, 38);
            this.btnSalir.TabIndex = 2;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // tmrPacman
            // 
            this.tmrPacman.Interval = 3;
            this.tmrPacman.Tick += new System.EventHandler(this.tmrPacman_Tick);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.BackColor = System.Drawing.Color.Transparent;
            this.label.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic);
            this.label.ForeColor = System.Drawing.Color.White;
            this.label.Location = new System.Drawing.Point(12, 406);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(85, 26);
            this.label.TabIndex = 3;
            this.label.Text = "Puntaje:";
            // 
            // lbMostrarPuntos
            // 
            this.lbMostrarPuntos.AutoSize = true;
            this.lbMostrarPuntos.BackColor = System.Drawing.Color.Transparent;
            this.lbMostrarPuntos.Font = new System.Drawing.Font("Harlow Solid Italic", 15.75F, System.Drawing.FontStyle.Italic);
            this.lbMostrarPuntos.ForeColor = System.Drawing.Color.White;
            this.lbMostrarPuntos.Location = new System.Drawing.Point(103, 406);
            this.lbMostrarPuntos.Name = "lbMostrarPuntos";
            this.lbMostrarPuntos.Size = new System.Drawing.Size(0, 26);
            this.lbMostrarPuntos.TabIndex = 4;
            // 
            // tmrRed
            // 
            this.tmrRed.Interval = 3;
            this.tmrRed.Tick += new System.EventHandler(this.tmrFantasmaRed_Tick);
            // 
            // tmrInicio
            // 
            this.tmrInicio.Enabled = true;
            this.tmrInicio.Interval = 5500;
            this.tmrInicio.Tick += new System.EventHandler(this.tmrPrincipal);
            // 
            // tmrBlue
            // 
            this.tmrBlue.Interval = 3;
            this.tmrBlue.Tick += new System.EventHandler(this.tmrBlueMover);
            // 
            // tmrPink
            // 
            this.tmrPink.Interval = 3;
            this.tmrPink.Tick += new System.EventHandler(this.tmrPinkMover);
            // 
            // tmrOrange
            // 
            this.tmrOrange.Interval = 3;
            this.tmrOrange.Tick += new System.EventHandler(this.tmrOrangeMover);
            // 
            // lbPreparado
            // 
            this.lbPreparado.AutoSize = true;
            this.lbPreparado.BackColor = System.Drawing.Color.Crimson;
            this.lbPreparado.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbPreparado.Font = new System.Drawing.Font("Harlow Solid Italic", 28F, System.Drawing.FontStyle.Italic);
            this.lbPreparado.ForeColor = System.Drawing.Color.Black;
            this.lbPreparado.Location = new System.Drawing.Point(360, 381);
            this.lbPreparado.Name = "lbPreparado";
            this.lbPreparado.Size = new System.Drawing.Size(212, 48);
            this.lbPreparado.TabIndex = 5;
            this.lbPreparado.Text = "Preparado?!";
            this.lbPreparado.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // tmrSacarFantasmas
            // 
            this.tmrSacarFantasmas.Interval = 10;
            this.tmrSacarFantasmas.Tick += new System.EventHandler(this.sacarFantasmas);
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tmrReiniciar
            // 
            this.tmrReiniciar.Interval = 1800;
            this.tmrReiniciar.Tick += new System.EventHandler(this.tmrReiniciarNivel);
            // 
            // tmrComer
            // 
            this.tmrComer.Interval = 1000;
            this.tmrComer.Tick += new System.EventHandler(this.tmrComiendo);
            // 
            // PantallaJugar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(934, 441);
            this.Controls.Add(this.lbPreparado);
            this.Controls.Add(this.lbMostrarPuntos);
            this.Controls.Add(this.label);
            this.Controls.Add(this.btnSalir);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "PantallaJugar";
            this.Text = "PacMan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CerrarVentana);
            this.Load += new System.EventHandler(this.pantJugLoad);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.pintarLaberinto);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyPressed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Timer tmrPacman;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label lbMostrarPuntos;
        private System.Windows.Forms.Timer tmrRed;
        private System.Windows.Forms.Timer tmrInicio;
        private System.Windows.Forms.Timer tmrBlue;
        private System.Windows.Forms.Timer tmrPink;
        private System.Windows.Forms.Timer tmrOrange;
        private System.Windows.Forms.Label lbPreparado;
        private System.Windows.Forms.Timer tmrSacarFantasmas;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer tmrReiniciar;
        private System.Windows.Forms.Timer tmrComer;
    }
}